/*
* This Algotithm is for EXACT COVER Problem
* EXACT COVER: From a given collection of subsets of a set S, we need to find subsets whose union is S and each element in S should be present only in one of these subsets
* Basically intersection is phi for each pair of subsets in Exact cover and union of all is S

* Let  {S}} = {N, O, P, E} be a collection of subsets of a set X = {1, 2, 3, 4} such that:
* N = { },
O = {1, 3},
P = {2, 3}, and
E = {2, 4}.
The subcollection {O, E} is an exact cover of X
* */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class AlgoritmX
{
    private static Scanner scanner;
    private static int size = 4;
    private static HashSet<Integer> set;
    private static int collectionSize = 3;
    private static ArrayList<HashSet<Integer>> collection;
    private static ArrayList<HashSet<Integer>> exactCover;
    
    static
    {
        scanner = new Scanner(System.in);
        set = new HashSet<>();
        collection = new ArrayList<>();
        exactCover = new ArrayList<>();
    }
    
    public static void main(String[] args)
    {
        input();
        if (!getExactCover())
            System.out.println("Solution does not exist");
        else
            printExactCover();
    }
    
    private static boolean getExactCover()
    {
        if (collection.isEmpty())
            return false;
        if (collection.size() == 1)
        {
            if (collection.get(0).size() != set.size())
            {
                return false;
            }
            else
            {
                collection.get(0).removeAll(set);
                return collection.get(0).size() == 0;
            }
        }
        for (int i = 0; i < collection.size(); i++)
        {
            HashSet<Integer> currentSet = collection.get(i);
            if (!set.containsAll(currentSet))
            {
                continue;
            }
            set.removeAll(currentSet);
            collection.remove(i);
            if (set.size() == 0 || getExactCover())
            {
                exactCover.add(currentSet);
                return true;
            }
            else
            {
                collection.add(currentSet);
                set.addAll(currentSet);
            }
        }
        return false;
    }
    
    private static void printExactCover()
    {
        for (HashSet<Integer> set : exactCover)
        {
            for (Integer integer : set)
            {
                System.out.print(integer + "\t");
            }
            System.out.println();
        }
    }
    
    private static void input()
    {
        System.out.print("Enter no of elements in set ");
        size = scanner.nextInt();
        System.out.println("Enter elements in set ");
        for (int i = 0; i < size; i++)
        {
            set.add(scanner.nextInt());
        }
        size = set.size();
        System.out.println("Enter no of subsets in collection");
        collectionSize = scanner.nextByte();
        
        for (int i = 0; i < collectionSize; i++)
        {
            System.out.println("Enter no of elements in subset " + (i + 1));
            int s = scanner.nextInt();
            System.out.println("Enter elements in subset " + (i + 1));
            HashSet<Integer> subset = new HashSet<>();
            if (s == 0)
                continue;
            for (int j = 0; j < s; j++)
            {
                subset.add(scanner.nextInt());
            }
            collection.add(subset);
        }
    }
}

/*
4
1 2 3 4
3
2 1 3
2 2 3
2 2 4
*/