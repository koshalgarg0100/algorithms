import java.util.Scanner;

public class NQueen
{
    private static int size;
    private static int[][] arr;
    private static int qPosInRow[]; //stores the position of queen placed in a row
    private static Scanner scanner = new Scanner(System.in);
    private static boolean isDone = false;
    
    public static void main(String[] args)
    {
        System.out.print("Enter size ");
        size = scanner.nextInt();
        arr = new int[size][size];
        qPosInRow = new int[size];
        nQueen();
    }
    
    private static void printArr()
    {
        System.out.println("");
        System.out.println("");
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                if (arr[i][j] >= 1)
                    System.out.print(arr[i][j] + "\t");
                else
                    System.out.print("0\t");
            }
            System.out.println("");
        }
    }
    
    private static void nQueen()
    {
        int curRow = 0;
        while (!isDone)
        {    //run till solution is not found
            int curCol = -1;
            for (int i = qPosInRow[curRow]; i < size; i++)
            {    //finds valid position in current row to place queen
                if (arr[curRow][i] == 0)
                {
                    curCol = i;
                    break;
                }
            }
            
            if (curCol == -1) //if no place available backtrack
            {
                if (curRow == 0) //no sol available
                {
                    isDone = false;
                    break;
                }
                place(curRow - 1, qPosInRow[curRow - 1], 1); //backtrack
                arr[curRow - 1][qPosInRow[curRow - 1]] = 0;
                qPosInRow[curRow] = 0;
                qPosInRow[curRow - 1]++;
                curRow--;
            }
            else
            {
                place(curRow, curCol, -1);
                arr[curRow][curCol] = 1;
                qPosInRow[curRow] = curCol;
                curRow++;
                if (curRow == size)
                    isDone = true;
            }
        }
        
        if (!isDone)
        {
            System.out.println("no sol exist");
        }
        else
        {
            printArr();
        }
    }
    
    private static void place(int row, int col, int count)
    {
        
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                if (arr[i][j] <= 0)
                {
                    if (i == row || j == col || (Math.abs(i - row) == Math.abs(j - col)))
                    {
                        if (!(i == row && j == col))
                            arr[i][j] += count;
                    }
                }
        
    }
    
}
